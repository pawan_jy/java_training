import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Event;
import java.applet.Applet;

public class Calc extends Applet {

    TextField t1;
    Label l;
    TextField t2;
    Button b;
    TextField t3;

    public void init(){
        t1 = new TextField(5);
        t2 = new TextField(5);
        t3 = new TextField(15);
        l = new Label("+");
        b = new Button("Add");

        add(t1);
        add(l);
        add(t2);
        add(b);
        add(t3);
    }
    
    public boolean action(Event e, Object o) {
        if(e.target.equals(b)) {
            t3.setText(null);
            t3.setText(t1.getText() + " + " + t2.getText() + " = " + (Integer.parseInt(t1.getText()) + Integer.parseInt(t2.getText())));
            t1.setText(null);
            t2.setText(null);
        }
        return false;
    }
}
