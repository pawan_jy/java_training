import java.applet.Applet;
import java.awt.Button;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Test extends Applet {

	Button b1,b2; // declaration of variables
	TextField tf;

	public void init() {// Begin lifecycle of applet

		add(b1= new Button("Click"));
		add(tf = new TextField(10));
		add(b2 = new Button("Clear"));

		Q q = new Q();
		q.hook(this);
        
		b1.addActionListener(new P(this));
		b2.addActionListener(q);
	}
}

class P implements ActionListener{

	Test a4;

	public P(Test o) {
		this.a4 = o;
	}

	public void actionPerformed(ActionEvent arg0) {
		a4.tf.setText("Hello World!");
	}
	
}

class Q implements ActionListener{

	Test a4;

	public void hook(Test o) {
		this.a4 = o;
	}

	public void actionPerformed(ActionEvent e) {
		a4.tf.setText("");
	}
}