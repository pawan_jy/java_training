import java.util.Scanner;

interface interfaceOne {
    default void say( final String str ){
        System.out.println( str );
    }

    public void hear();
}

interface interfaceTwo {
    default void displayResult(final Float f){
        System.out.println( f );
    }

    public void add();
}

interface interfaceThree extends interfaceTwo {
    default void display(){
        System.out.println("Displayed from the third interface.");
    }
}

public class Interface implements interfaceOne, interfaceThree {

    static Scanner sc = new Scanner(System.in);

    public void showInterface() {
        hear();
        add();
        interfaceThree.super.display();
    }
    public static void main(String[] args) {
        Interface demoInterface = new Interface();
        demoInterface.showInterface();
        sc.close();
    }
    @Override
    public void hear() {
        System.out.println("Write something:");
        String line = sc.nextLine();
        interfaceOne.super.say( line );
    }
    @Override
    public void add() {
        System.out.println("Write first number:");
        Float a = sc.nextFloat();
        System.out.println("Write second number:");
        Float b = sc.nextFloat();
        Float c = a + b;
        displayResult( c );
    }
}