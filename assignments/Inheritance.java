public class Inheritance {
    public static void main(String[] args) {
        System.out.println("Hi! This line is from main.");

        
        System.out.println("\n------------------=Parent Class=---------------------");
        Parent p = new Parent();

        System.out.println(p.parent + " - accessed from " + Parent.str + " object");

        p.parentSay();


        System.out.println("\n------------------=Child Class=---------------------");
        Child c = new Child();

        System.out.println(c.parent + " - accessed from " + Child.str + " object");
        System.out.println(c.child + " - accessed from " + Child.str + " object");

        c.parentSay();
        c.childSay();


        System.out.println("\n------------------=Type Casting=---------------------");
        Parent p1 = new Child();
        p1.parentSay();
        // ((Child) p1).childSay();

    }
}

class Parent {
    static String str = "Parent";
    int parent = 10;
    void parentSay() {
        System.out.println("This is from the Parent Class.");
    }
}

class Child extends Parent {
    static String str = "Child";
    int child = 5;
    void parentSay() {
        System.out.println("This line is from Parent's Method of Child Class.");
    }
    void childSay() {
        System.out.println("This line is from Child's Method of Child Class.");
    }
}
