public class Variables {

    static int a;
    static byte b;
    static short c;
    static long d;
    static float e;
    static double f;
    static char g;
    static boolean h;

    int p;
    byte q;
    short r;
    long s;
    float t;
    double u;
    char v;
    boolean w;
    
    public static void main(String[] args) {

        System.out.println("int: " + a);
        System.out.println("class int: " + Variables.a);
        System.out.println(" ");

        System.out.println("byte: " + b);
        System.out.println("class byte: " + Variables.b);
        System.out.println(" ");

        System.out.println("short: " + c);
        System.out.println("class short: " + Variables.c);
        System.out.println(" ");

        System.out.println("long: " + d);
        System.out.println("class long: " + Variables.d);
        System.out.println(" ");

        System.out.println("float: " + e);
        System.out.println("class float: " + Variables.e);
        System.out.println(" ");

        System.out.println("double: " + f);
        System.out.println("class double: " + Variables.f);
        System.out.println(" ");

        System.out.println("char: " + g);
        System.out.println("class char: " + Variables.g);
        System.out.println(" ");

        System.out.println("bool: " + h);
        System.out.println("class bool: " + Variables.h);
        System.out.println(" ");

        System.out.println("Instance Variables:");
        
        Variables ins = new Variables();

        System.out.println("int: " + ins.p);
        System.out.println("byte: " + ins.q);
        System.out.println("short: " + ins.r);
        System.out.println("long: " + ins.s);
        System.out.println("float: " + ins.t);
        System.out.println("double: " + ins.u);
        System.out.println("char: " + ins.v);
        System.out.println("bool: " + ins.w);

        System.out.println(ins);

        Variables.myMethod();
    }

    static void myMethod() {
        System.out.println("\nhello!");

        System.out.println(myClass.x);

        myClass example1 = new myClass();
        
        System.out.println(example1);
        System.out.println(example1.y);
    }
}

class myClass {
    static int x;
    int y;
}
