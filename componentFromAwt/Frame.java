import java.awt.*;



class Form {
    Form() {
        Frame f = new Frame();
        Label l = new Label("Type here:");
        Button b = new Button("Submit");
        TextField t = new TextField();
        
        l.setBounds(20, 30, 80, 30);
        t.setBounds(20, 70, 210, 30);
        b.setBounds(180, 120, 50, 30);

        f.add(b);
        f.add(l);
        f.add(t);
      
        f.setSize(250,250);
        f.setTitle("Form");
        f.setLayout(null);
        f.setVisible(true);  
}
    public static void main(String args[]) {
        Form frmObj= new Form();

        System.out.println(frmObj.getClass().toString());
    }

}