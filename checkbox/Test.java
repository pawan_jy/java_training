import java.applet.Applet;
import java.awt.Checkbox;
import java.awt.Event;
import java.awt.TextField;

public class Test extends Applet {

    TextField t;
    Checkbox cb1, cb2, cb3, cb4;

    int sum;

    public void init(){

        setLayout(null);

        t = new TextField(15);
        t.setText("0");
        t.setBounds(20, 120, 160, 20);

        cb1 = new Checkbox("1", false);
        cb2 = new Checkbox("2", false);
        cb3 = new Checkbox("3", false);
        cb4 = new Checkbox("4", false);

        cb1.setBounds(87, 20, 25, 20);
        cb2.setBounds(87, 40, 25, 20);
        cb3.setBounds(87, 60, 25, 20);
        cb4.setBounds(87, 80, 25, 20);
        
        add(cb1);
        add(cb2);
        add(cb3);
        add(cb4);
        add(t);

    }

    public boolean action(Event e, Object o) {
        sum = 0;
        if(cb1.getState()) {
            sum = sum + 1;
        }
        if(cb2.getState()) {
            sum = sum + 2;
        }
        if(cb3.getState()) {
            sum = sum + 3;
        }
        if(cb4.getState()) {
            sum = sum + 4;
        }
        t.setText(Integer.toString(sum));
        return false;
    }
}