import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * hashMap
 */
public class hashMapDemo {

    public static void main(String[] args) {
        Map <String, Double> hm = new HashMap<String, Double>();
        hm.put("Doe", (double) 3434.4);
        hm.put("Tom", (double) -34.4);
        hm.put("Jim", (double) 7000);
        hm.put("Joe", (double) 10000);
        hm.put("John", (double) 8999);

        Set set= hm.entrySet();

        Iterator i = set.iterator();

        while (i.hasNext()) {
            Map.Entry m = (Map.Entry) i.next();
            System.out.println(m.getKey() + " : " + m.getValue());
        }

        double bal = hm.get("Doe");
        System.out.println(bal);

        hm.put("Doe", bal+1000);

        bal = hm.get("Tom");
        hm.replace("Tom", bal + 100);  //works like charm

        bal = hm.get("Doe");
        System.out.println(bal);

        bal = hm.get("Tom");
        System.out.println(bal);
    }
}