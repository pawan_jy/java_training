import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

public class TwoDArrays extends Frame implements ActionListener {

    Button[][] buttons;

    public TwoDArrays(int dim, int size) {

        setLayout(new GridLayout(dim, dim));
        setSize(dim * size, (int)(dim * size * 1.15));

        buttons = new Button[dim][dim];

        for (int i = 0; i < buttons.length; i++) {
            for (int j = 0; j < buttons[i].length; j++) {
                buttons[i][j] = new Button();
                buttons[i][j].setLabel(Math.random()<0.5?"X":"O");
                add(buttons[i][j]);
                buttons[i][j].addActionListener(this);
            }
        }

        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                System.exit(0);
            }
        });

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();

        if (obj instanceof Button){
            Button b = (Button) obj;
            
            if (b.getLabel().contains("X"))
                b.setLabel("O");
            else
                b.setLabel("X");
        }
    }
    public static void main(String[] args) {
        new TwoDArrays(6,70);
    }
}
