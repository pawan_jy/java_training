import java.util.Objects;

public class MyStudent {

    private int id;
    private String name;

    public MyStudent(){}
    public MyStudent(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getID(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }

    public void setID(int id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }

    @Override
    public int hashCode(){
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o){
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (this.getClass() != o.getClass())
            return false;
        Student other = (Student) o;
        return Objects.equals(this.name, other.name);
    }

    public static void main(String[] args) {
        
    }

}
