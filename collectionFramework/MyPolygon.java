import java.applet.Applet;
import java.awt.Graphics;


public class MyPolygon extends Applet {

    int arrLength;
    int xPoints[];
    int yPoints[];

    public void init(){
        arrLength = 6;

        xPoints = new int[arrLength];
        yPoints = new int[arrLength];

        for (int i = 0; i < arrLength; i++) {
            xPoints[i] = (int) (Math.random() * 235);
            yPoints[i] = (int) (Math.random() * 220);
        }

    }

    public void paint(Graphics grphx){
        // int xPoints[] = {30, 120, 140, 50, 190};
        // int yPoints[] = {50, 60, 160, 140, 80};

        grphx.drawPolygon(this.xPoints, this.yPoints, (this.xPoints.length == this.yPoints.length)? this.yPoints.length: 0 );
    }
}