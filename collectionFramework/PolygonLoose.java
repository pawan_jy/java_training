import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.Graphics;
import java.awt.Polygon;
import java.lang.Math;

public class PolygonLoose extends Frame {
    
    Polygon plgn;

    public  PolygonLoose(int[] x, int[] y, int i, int j) {
        
        plgn = new Polygon(x,y, x.length);
        
        setSize(i, j);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                System.out.println("Close");
                dispose();
            }
            public void windowClosed (WindowEvent e) {
                System.out.println("Kill");
                System.exit(0);
            }
        });

    }

    public void paint(Graphics g){
        g.drawPolygon(plgn);
    }

    public static void main() {

        System.out.println(":-o");
    }
    public static void main(String[] args) {

        int arrLength = 3;

        int frameHight = 355;
        int frameWidth = 345;
        int padding = 50;

        int xPoints[] = new int[arrLength];
        int yPoints[] = new int[arrLength];

        for (int i = 0; i < arrLength; i++) {
            xPoints[i] = (int) (Math.random() * (frameWidth - padding)) + (padding/2);
            yPoints[i] = (int) (Math.random() * (frameHight - padding)) + (padding/2);
        }

        new PolygonLoose( xPoints, yPoints, frameWidth, frameHight);

        main();
        main(arrLength);
    }

    public static void main(int i) {
        System.out.println("Total no of points in the polygon is: " + i);
    }
}
