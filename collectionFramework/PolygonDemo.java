import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.Graphics;
import java.awt.Polygon;

public class PolygonDemo extends Frame {

    Polygon plgn;

    public  PolygonDemo(int[] x, int[] y) {
        
        plgn = new Polygon(x,y, x.length);
        
        setSize(345, 355);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                System.out.println("Close");
                dispose();
            }
            public void windowClosed (WindowEvent e) {
                System.out.println("Kill");
                System.exit(0);
            }
        });

    }

    public void paint(Graphics g){
        g.drawPolygon(plgn);
    }

    public static void main() {

        System.out.println(":-o");
    }
    public static void main(String[] args) {

        int xPoints[] = {40, 180, 200, 70, 270};
        int yPoints[] = {70, 90, 230, 200, 120};

        if (xPoints.length == yPoints.length){
            new PolygonDemo(xPoints,yPoints);
        } else {
            System.err.println("Arrays are not equal.");
        }

        main();
        main(6);
    }

    public static void main(int i) {

        System.out.println(i);
    }
}
