import java.util.Vector;

public class MyVector {

    public static void main(String[] args) {
        Vector<String> v1 = new Vector<String>();

        String s1 = new String("Nogen");
        String s2 = new String("Nogen");

        v1.add(s1);
        // v1.add(s2);
        
        System.out.println(v1);
        System.out.println(v1.contains(s2));


        Vector<Student> v2 = new Vector<Student>(){
            @Override
            public boolean contains(Object o){
                return this.toString().contains(o.toString());
            }
        };

        Student st1 = new Student(1 , "Nogen");
        Student st2 = new Student(1 , "Nogen");

        v2.add(st1);
        // v2.add(st2);

        System.out.println(v2);
        System.out.println(v2.contains(st2));
    }
}

class Student {
    int id;
    String name;

    public Student(int id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString(){
        return "Student [id: " + this.id + " name: " + this.name + "]";
    }
}
