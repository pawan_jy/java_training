import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TicTacToe extends Frame {

    Button buttons[][];

    public TicTacToe(int dim, int x){

        setLayout(new GridLayout(dim, dim));

        buttons = new Button[dim][dim];

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                buttons[i][j] = new Button();
                buttons[i][j].setLabel(Math.random()<0.5?"X":"O");
                add(buttons[i][j]);
                buttons[i][j].addActionListener(new Toggle(buttons[i][j]));
            }
        }

        setSize(x, (int)(x * 1.15));
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                System.out.println("Close");
                dispose();
            }
            public void windowClosed (WindowEvent e) {
                System.out.println("Kill");
                System.exit(0);
            }
        });

    }

    public static void main(String[] args) {
        int dim = 4;

        new TicTacToe(dim, dim * 70);
    }
}

class Toggle implements ActionListener {
    Button btn;

    public Toggle(Button b){
        this.btn = b;
    }
    public void actionPerformed(ActionEvent e) {
        if (btn.getLabel() == "X") {
            btn.setLabel("O");
        } else {
            btn.setLabel("X");
        }
    }

}
