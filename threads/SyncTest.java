class PrinterClass {
    public synchronized void myMethod(String s){
    // public void myMethod(String s){

        System.out.println("\n" + s +": wait 100 milliseconds to complete.");

        try {
            Thread.sleep(100);
        } catch (Exception e) {
            //TODO: handle exception
        }

        System.out.println("complete...\n");
    }

}

class UserClass extends Thread {

    PrinterClass myPrinter;
    String s;

    public UserClass(PrinterClass prntr) {
        this.myPrinter = prntr;
        start();
        // run();
    }

    public void run(){
        myPrinter.myMethod(getName());
    }
}

public class SyncTest {
    public static void main(String[] args) {

        PrinterClass printer;
        UserClass printJob1, printJob2;

        printer = new PrinterClass();

        printJob1 = new UserClass(printer);
        printJob1.setName("printJob1");

        printJob2 = new UserClass(printer);
        printJob2.setName("printJob2");

    }
}
