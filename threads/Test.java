public class Test implements Runnable {

    public void run() {
        for (int i = 0; i < 10000000; i++) {
            if (i % 100000 == 0) {
                System.out.println("Implements");
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    //TODO: handle exception
                }
            }
        }
    }

    public static void main(String[] args) {

        Test t;
        Th1 th1;

        t = new Test();
        th1 = new Th1();
        
        // t.start()   //there isn't any
        Thread th2 = new Thread(t);  // need a new Thread object to call start()

        System.out.println(th1.getName());
        System.out.println(th2.getName());

        th2.start();
        th1.start();
        // th1.run(); // won't run concurrently

    }
}

class Th1 extends Thread  {

    public void run() {
        for (int i = 0; i < 10000000; i++) {
            if (i % 100000 == 0) {
                System.out.println("Extends");
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    //TODO: handle exception
                }
            }
        }
    }
}