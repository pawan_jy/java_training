import java.applet.Applet;
import java.awt.TextField;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Test extends Applet implements ActionListener {
	
	TextField tf;
	Button b1, b2;
	
	public void init() {
		b1 = new Button("Click");
		b2 = new Button("Clear");
		tf = new TextField(5);
		
		add(b1);
		add(tf);
		add(b2);

		b1.addActionListener(this);
		b2.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == b1){
			tf.setText("Hi");
		}
		if(e.getSource() == b2){
			tf.setText("");
		}
	}
}
