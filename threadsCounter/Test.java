import java.awt.Frame;
import java.awt.TextField;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;


public class Test extends Frame {

    TextField t1, t2;
    ThreadGroup thGrp;
    Thread th1, th2;

    public class InnerTest extends Thread {
        public InnerTest(ThreadGroup Grp, String thName) {
            super(Grp, thName);
        }

        public void run(){
            for (int i = 0; true; i++) {
                t2.setText(String.valueOf(i));
                if (i == 500) {
                    i = 0;
                }
                try {
                    Thread.sleep(5);
                } catch (Exception e) {
                    //TODO: handle exception
                }
            }
            // int i = 0;
            // while (true) {
            //     t2.setText(String.valueOf(i));
            //     if (i == 500) {
            //         i = 0;
            //     } else {
            //         i++;
            //     }

            //     try {
            //         Thread.sleep(50);
            //     } catch (Exception e) {
            //         //TODO: handle exception
            //     }
            // }
        }
    }
    
    public Test() {

        setLayout( new FlowLayout());

        t1 = new TextField(7);
        t2 = new TextField(7);

        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                System.out.println("Close");
                dispose();
            }
            public void windowClosed (WindowEvent e) {
                System.out.println("Kill");
                System.exit(0);
            }
        });
        
        setSize(200, 200);
        setVisible(true);
        
        add(t1);
        add(t2);

        thGrp = new ThreadGroup( "Counters" );

        Runnable rr  = () -> {
            int i = 0;
                do {
                    t1.setText(String.valueOf(i));
                    if (i == 79) {
                        i = 0;
                    } else {
                        i++;
                    }
    
                    try {
                        Thread.sleep(50);
                    } catch (Exception e) {
                        //TODO: handle exception
                    }
                } while (true);
        };

        th1 = new Thread(thGrp, rr, "TextBox_1");

        // th1 = new Thread(thGrp, new Runnable() {
        //     public void run(){
        //         int i = 0;
        //         do {
        //             t1.setText(String.valueOf(i));
        //             if (i == 15) {
        //                 i = 0;
        //             } else {
        //                 i++;
        //             }
    
        //             try {
        //                 Thread.sleep(500);
        //             } catch (Exception e) {
        //                 //TODO: handle exception
        //             }
        //         } while (true);
        //         // while (true) {
        //         //     t1.setText(String.valueOf(i));
        //         //     if (i == 15) {
        //         //         i = 0;
        //         //     } else {
        //         //         i++;
        //         //     }
    
        //         //     try {
        //         //         Thread.sleep(50);
        //         //     } catch (Exception e) {
        //         //         //TODO: handle exception
        //         //     }
        //         // }
        //     }
        // }, "TextBox_1" );
        th2 = new InnerTest( thGrp, "TextBox_2");
        
        th1.getPriority();
        th2.getState();
        th1.start();
        th2.start();
        System.out.println(th1.toString());
        System.out.println(th2.toString());
        System.out.println(thGrp.toString());
    }
    public static void main(String[] args) {
        new Test();
    }
}