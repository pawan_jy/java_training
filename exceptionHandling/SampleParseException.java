import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class SampleParseException {
    static void convertDateFormat(String inputDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
            Date date = sdf.parse(inputDate);

            SimpleDateFormat outputsdf = new SimpleDateFormat("yyyy-MM-dd");
            String outputdate = outputsdf.format(date);
            System.out.println("\nInput: " + inputDate);
            System.out.println("Output: " + outputdate + "\n");
        } catch (java.text.ParseException e) {
            System.out.println("Some esception occured.");
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter date(dd/mm/yyyy): ");
        String inputDate = sc.nextLine();
        convertDateFormat(inputDate);
        sc.close();
    }
}
