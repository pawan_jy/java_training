import java.util.Scanner;

public class SampleException {

    // use of throw keyword
    static void demoThrow() {
        try {
            throw new ArithmeticException("Demo");
        } catch (Exception e) {
            System.out.println("Exception in the demoThrow() method.");
        }
    }
    
    // use of throws keyword
    static void demoThrows() throws Exception {
        try {
            System.out.println("Inside demoThrows() method-");
            throw new NumberFormatException("Demo Exception");
        } catch (Exception e) {
            System.out.println("\tNumber Format Exception is thrown.");
        }
    }

    // Using Custom Exception
    static void validateInput(int number) throws InvalidInputException {
        if (number > 100) {
            throw new InvalidInputException("Number is not valid input.");
        }
        System.out.println("Input is valid.");
    }
    public static void main(String[] args) {

        // Logic for custom Exception
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number less than 100:");
        int number = sc.nextInt();
        try {
            validateInput(number);
        } catch (InvalidInputException e) {
            System.out.println("Exception Occured: Number is greater than 100.");
        }
        sc.close();

        // Checked Exception which can't be ignored
        // String str = null;
        // System.out.println(str.length());

        // Ignorable exceptions
        // Arithmatic Exception
        try {
            demoThrows();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int a = 30, b = 0;
            int c = a/b;

            System.out.println("result:" + c);
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println("Can't divide by Zero(0)!!");
        }

        // Number Format Exception
        try {
            int num = Integer.parseInt("Sample String!!");

            System.out.println(num);
        } catch (Exception e) {
            // e.printStackTrace();
            demoThrow();
        }
        finally {
            System.out.println("All Exceptions are caught.");
        }
    }
}

// Defining Custom Exception
class InvalidInputException extends Exception {
    InvalidInputException(String str){
        super(str);
    }
}