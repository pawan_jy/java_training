import java.awt.Frame;
import java.awt.TextField;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;


class Tex extends TextField implements Runnable {

    public Tex(int i){
        super(i);
    }

    public void run(){
        for (int k = 0; k < 5000; k++) {
            this.setText(Integer.toString(k));
            if (k == 4999){
                k = 0;
            }
            try {
                Thread.sleep(50);
            } catch (Exception e) {
                //TODO: handle exception
            }
        }
    }
}

public class Test extends Frame {

    Tex t1, t2;
    Thread th1, th2;

    public Test() {

        setLayout( new FlowLayout());

        t1 = new Tex(7);
        t2 = new Tex(7);

        addWindowListener(new WindowAdapter() {
            public void windowClosing (WindowEvent e) {
                System.out.println("Close");
                dispose();
            }
            public void windowClosed (WindowEvent e) {
                System.out.println("Kill");
                System.exit(0);
            }
        });
        
        setSize(200, 200);
        setVisible(true);
        
        add(t1);
        add(t2);

        th1 = new Thread(t1);
        th2 = new Thread(t2);
        th1.start();
        th2.start();
    }

    public static void main(String[] args) {
        new Test();
    }
}