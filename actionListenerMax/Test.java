import java.applet.Applet;
import java.awt.Button;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Test extends Applet {
	
	Button b1, b2;
	TextField tf;
	
	public void init() {
		tf = new TextField(10);

		add(b1 = new Button("Click"));
		add(tf);
		add(b2 = new Button("CLear"));

		b1.addActionListener(new P());
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tf.setText("");
			}
		});
	}

	class P implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			tf.setText("Hi");
		}

	}
}