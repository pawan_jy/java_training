package com.jpacrud.client;

import com.jpacrud.entities.Student;
import com.jpacrud.service.StudentService;
import com.jpacrud.service.StudentServiceImpl;

public class Client {

	public static void main(String[] args) {
		StudentService service = new StudentServiceImpl();
		Student student = new Student();
		
		// Create Operation
		student.setStudentId(10210);
		student.setDeptName("MECH");
		student.setFirstName("Vishal");
		System.out.println("Student Id: " + student.getStudentId());
		System.out.println("Student Name: " + student.getFirstName());
		System.out.println("Department: " + student.getDeptName());
		service.addStudent(student);		// put info in database
		System.out.println("Record Added to Database.");
		System.out.println("-----------------------------------------------------------\n");
		
		
		// Retrieve Operation
		student = service.findStudentById(10203);
		System.out.println("Student Id: " + student.getStudentId());
		System.out.println("Student Name: " + student.getFirstName());
		System.out.println("Department: " + student.getDeptName());
		System.out.println("Record Retrieved from Database.");
		System.out.println("-----------------------------------------------------------\n");
		
		// Update Operation
		student = service.findStudentById(10210);
		System.out.println("Student Id: " + student.getStudentId());
		System.out.println("Student Old Name: " + student.getFirstName());
		student.setFirstName("Vicky");		// change the name
		System.out.println("Student New Name: " + student.getFirstName());
		System.out.println("Department: " + student.getDeptName());
		service.updateStudent(student);		// put changed info in database
		System.out.println("Record Updated in Database.");
		System.out.println("-----------------------------------------------------------\n");
		
		// Delete Operation
		student = service.findStudentById(10201);
		System.out.println("Student Id: " + student.getStudentId());
		System.out.println("Student Name: " + student.getFirstName());
		System.out.println("Department: " + student.getDeptName());
		service.removeStudent(student);
		System.out.println("Record Deleted from Database.");
		System.out.println("-----------------------------------------------------------\n");
		
		System.out.println("Operations Completed Successfully");
		
	}

}
