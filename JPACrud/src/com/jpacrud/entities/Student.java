package com.jpacrud.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="students")
public class Student implements Serializable {
    
	private static final long serialVersionUID = 1L;
	
	// Attributes according to database table
	@Id
	@Column(name="Student_ID")
    private int studentId;					// PRIMARY KEY
	
	@Column(name="Department")
    private String deptName;
	
	@Column(name="First_Name")
    private String firstName;
    
    // Getters and Setters
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

}
