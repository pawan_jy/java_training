package com.jpacrud.service;

import com.jpacrud.dao.StudentDao;
import com.jpacrud.dao.StudentDaoImpl;
import com.jpacrud.entities.Student;

public class StudentServiceImpl implements StudentService {
	
	// Attributes
	private StudentDao dao;
	
	//Constructor
	public StudentServiceImpl() {
		dao = new StudentDaoImpl();
	}

	@Override
	public void addStudent(Student student) {
		dao.beginTransaction();
		dao.addStudent(student);
		dao.commitTransaction();
	}

	@Override
	public Student findStudentById(int id) {
		//No need for transaction, as it's a read operation
		Student student = dao.getStudentById(id); 		
		return student;
	}

	@Override
	public void updateStudent(Student student) {
		dao.beginTransaction();
		dao.updateStudent(student);
		dao.commitTransaction();
	}

	@Override
	public void removeStudent(Student student) {
		dao.beginTransaction();
		dao.removeStudent(student);
		dao.commitTransaction();
	}

}
