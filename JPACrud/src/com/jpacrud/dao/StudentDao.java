package com.jpacrud.dao;

import com.jpacrud.entities.Student;

public interface StudentDao {
	
	public abstract void beginTransaction();
	public abstract void commitTransaction();
	
	// Create
	public abstract void addStudent(Student student);
	// Read
	public abstract Student	getStudentById(int id);
	// Update
	public abstract void updateStudent(Student student);
	//Delete
	public abstract void removeStudent(Student student);
	
}
