import java.applet.Applet;
import java.awt.Choice;
import java.awt.Event;
import java.awt.TextField;

public class Test extends Applet{
    TextField tf;
    Choice ch;
    
    public void init() {
        ch = new Choice();
        tf = new TextField("Your choice will appear here");

        ch.add("Option 1");
        ch.add("Option 2");
        ch.add("Option 3");
        ch.add("Option 4");
        ch.add("Option 5");

        add(ch);
        add(tf);
    }
    public boolean action(Event e, Object o) {
        if(e.target.equals(ch))
            tf.setText("You have selected " + ch.getSelectedItem());
            return false;
    }
}
